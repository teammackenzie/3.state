import React, { Component } from 'react';
import Ingreso from './Ingreso';

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			clicks: this.props.cantidad,
			palabra: ''
		};
		//this.handlerIncrementar = this.handlerIncrementar.bind(this);
	}
	handlerIncrementar = () => {
		this.setState({ clicks: this.state.clicks + 1 });
	};
	handlerChangeTexto = palabra => this.setState({ palabra });

	render() {
		const { clicks, palabra } = this.state;
		return (
			<div className="container m-2 p-2">
				<button onClick={this.handlerIncrementar} type="button" className="btn btn-primary">
					Notifications <span className="badge badge-light">{clicks}</span>
				</button>
				<br />
				<div className="alert alert-success" role="alert">
					{palabra}
				</div>
				<Ingreso handlerChangeTexto={this.handlerChangeTexto} />
			</div>
		);
	}
}
