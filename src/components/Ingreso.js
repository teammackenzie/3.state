import React, { Component } from 'react';

export default class Ingreso extends Component {
	state = {
		palabra: ''
	};
	handleChangeInput = event => {
		//console.log(`palabra antes: ${this.state.palabra}`);
		this.setState({ palabra: event.target.value }, () => {
			this.props.handlerChangeTexto(this.state.palabra);
		});

		// setTimeout(() => {
		// 	console.log(`palabra despues: ${this.state.palabra}`);
		// }, 100);
	};
	handleClearTexto = () => {
		this.setState({ palabra: '' }, () => {
			this.props.handlerChangeTexto(this.state.palabra);
		});
	};
	handleSendTexto = () => {
		this.props.handlerChangeTexto(this.state.palabra);
	};
	render() {
		return (
			<React.Fragment>
				<div className="input-group mb-3">
					<div className="input-group-prepend">
						<span className="input-group-text" id="basic-addon1">
							Texto
						</span>
					</div>
					<input
						value={this.state.palabra}
						onChange={this.handleChangeInput}
						type="text"
						className="form-control"
						placeholder="Username"
						aria-label="Username"
						aria-describedby="basic-addon1"
					/>
				</div>
				<div className="alert alert-success" role="alert">
					{this.state.palabra}
				</div>
				<button onClick={this.handleClearTexto} type="button" className="btn btn-primary">
					Limpiar
				</button>
				<button onClick={this.handleSendTexto} type="button" className="btn btn-secondary">
					Buscar
				</button>
			</React.Fragment>
		);
	}
}
